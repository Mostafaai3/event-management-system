﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.Core.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StateId { get; set; }
        public List<Event>? Events { get; set; }
    }
}
