﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.Core.Dto
{
    public class SearchDto
    {
        public List<int>? Categories { get; set; }
        public List<int>? Locations { get; set; }
    }
}
