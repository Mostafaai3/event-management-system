﻿using EventManagementSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.Core.Dto
{
    public class EventDetailsDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int LocationId { get; set; }
        public Location Location { get; set; }
        public int Price { get; set; }
        public int TicketsNumber { get; set; }
        public DateTime AdvertisingDate { get; set; }
        public DateTime Date { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
}
