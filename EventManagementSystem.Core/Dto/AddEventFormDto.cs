﻿using EventManagementSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.Core.Dto
{
    public class AddEventFormDto
    {
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<State> Locations { get; set; }
    }
}
