﻿using EventManagementSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.Core.Dto
{
    public class AddEventDto
    {
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Description { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int LocationId { get; set; }
        [Required]
        public int Price { get; set; }
        [Required]
        public int TicketsNumber { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
}
