﻿using EventManagementSystem.Core.Dto;
using EventManagementSystem.Core.Models;
using EventManagementSystem.EF.UOW;
using Humanizer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;

namespace EventManagementSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EventsController(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("GetInLocation/{id}")]
        public async Task<ActionResult<IEnumerable<Event>>> GetInLocation(int id)
        {
            if (await _unitOfWork.Locations.GetByIdAsync(id) is null) return NotFound();
            var events = await _unitOfWork.Events.FindAllAsync(e => e.LocationId == id);
            return Ok(events);
        }

        [HttpGet("GetInCategory/{id}")]
        public async Task<ActionResult<IEnumerable<Event>>> GetInCategory(int id)
        {
            if (await _unitOfWork.Categories.GetByIdAsync(id) is null) return NotFound();
            var events = await _unitOfWork.Events.FindAllAsync(e => e.CategoryId == id);
            return Ok(events);
        }

        [HttpGet("GetByUser/{id}")]
        public async Task<ActionResult<IEnumerable<Event>>> GetByUser(string id)
        {
            if (await _unitOfWork.Users.FindAsync(u => u.Id == id) is null) return NotFound();
            var events = await _unitOfWork.Events.FindAllAsync(e => e.UserId == id);
            return Ok(events);
        }

        [HttpGet("GetEventDetails/{id}")]
        public async Task<ActionResult<EventDetailsDto>> GetEventDetails(int id)
        {
            if (await _unitOfWork.Events.GetByIdAsync(id) is null) return NotFound();
            var eve = await _unitOfWork.Events.GetEventDetails(id);
            return Ok(eve);
        }

        [HttpGet("GetForm")]
        public async Task<ActionResult<AddEventFormDto>> GetForm()
        {
            var categories = await _unitOfWork.Categories.GetAllAsync();
            var locations = await _unitOfWork.States.GetAllLocationsAsync();
            var form = new AddEventFormDto
            {
                Categories = categories,
                Locations = locations
            };
            return Ok(form);
        }

        [Authorize]
        [HttpPost("AddEvent")]
        public async Task<ActionResult<Event>> AddEvent(AddEventDto dto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (await _unitOfWork.Locations.GetByIdAsync(dto.LocationId) is null)
                return BadRequest("Location not found");
            if (await _unitOfWork.Categories.GetByIdAsync(dto.CategoryId) is null)
                return BadRequest("Type not found");
            var eve = new Event
            {
                Name = dto.Name,
                Description = dto.Description,
                Date = dto.Date,
                AdvertisingDate = DateTime.Now,
                CategoryId = dto.CategoryId,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                LocationId = dto.LocationId,
                Price = dto.Price,
                TicketsNumber = dto.TicketsNumber,
            };
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _unitOfWork.Users.FindAsync(u => u.UserName.Equals(userName));
            eve.UserId = user.Id;
            await _unitOfWork.Events.AddAsync(eve);
            await _unitOfWork.SaveChangesAsync();
            return CreatedAtAction("GetEventDetails", new { id = eve.Id }, eve);
        }

        [Authorize]
        [HttpDelete("DeleteEvent/{id}")]
        public async Task<ActionResult> DeleteEvent(int id)
        {
            var eve = await _unitOfWork.Events.GetByIdAsync(id);
            if (eve is null)
                return NotFound();
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _unitOfWork.Users.FindAsync(u => u.UserName.Equals(userName));
            if (eve.UserId != user.Id) return BadRequest();
            _unitOfWork.Events.Delete(eve);
            await _unitOfWork.SaveChangesAsync();
            return NoContent();
        }

        [Authorize]
        [HttpPut("UpdateEvent/{id}")]
        public async Task<ActionResult> UpdateEvent(int id, AddEventDto dto)
        {
            var eve = await _unitOfWork.Events.GetByIdAsync(id);
            if (eve is null)
                return NotFound();
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _unitOfWork.Users.FindAsync(u => u.UserName.Equals(userName));
            if (eve.UserId != user.Id) return BadRequest();
            eve.Name = dto.Name;
            eve.Description = dto.Description;
            eve.Date = dto.Date;
            eve.Price = dto.Price;
            eve.Latitude = dto.Latitude;
            eve.Longitude = dto.Longitude;
            eve.CategoryId = dto.CategoryId;
            eve.LocationId = dto.LocationId;
            eve.Price = dto.Price;
            eve.TicketsNumber = dto.TicketsNumber;
            _unitOfWork.Events.Update(eve);
            await _unitOfWork.SaveChangesAsync();
            return NoContent();
        }

        [HttpPost("SearchEvent")]
        public async Task<ActionResult> SearchEvent(SearchDto dto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var events =await _unitOfWork.Events.SearchEvent(dto);
            return Ok(events);
        }

        [Authorize]
        [HttpGet("GetBookedEvents")]
        public async Task<ActionResult<IEnumerable<Event>>> GetBookedEvents()
        {
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _unitOfWork.Users.FindAsync(u => u.UserName.Equals(userName));
            var events = await _unitOfWork.Events.GetBookedEvents(user.Id);
            await _unitOfWork.SaveChangesAsync();
            return Ok(events);
        }
    }
}
