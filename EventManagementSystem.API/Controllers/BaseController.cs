﻿using AutoMapper;
using EventManagementSystem.Core.Models;
using EventManagementSystem.EF.UOW;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.Metrics;

namespace EventManagementSystem.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public BaseController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet("GetAllLocations")]
        public async Task<ActionResult<IEnumerable<State>>> GetAllLocations()
        {
            var states = await _unitOfWork.States.GetAllLocationsAsync();
            if (states is null) return NotFound();
            return Ok(states);
        }

        [HttpGet("GetAllCategories")]
        public async Task<ActionResult<IEnumerable<Category>>> GetAllCategories()
        {
            var categories = await _unitOfWork.Categories.GetAllAsync();
            if (categories is null) return NotFound();
            return Ok(categories);
        }

        [HttpGet("SearchUser")]
        public async Task<ActionResult<IEnumerable<User>>> SearchUser(string term)
        {
            var user = await _unitOfWork.Users.FindAllAsync(u => u.UserName.Contains(term) || u.FirstName.Contains(term) || u.LastName.Contains(term));
            return Ok(user);
        }
    }
}
