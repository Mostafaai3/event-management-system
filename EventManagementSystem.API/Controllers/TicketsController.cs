﻿using EventManagementSystem.Core.Models;
using EventManagementSystem.EF.UOW;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace EventManagementSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TicketsController(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }

        [Authorize]
        [HttpGet("Book/{eventId}")]
        public async Task<ActionResult> Book(int eventId)
        {
            var eve = await _unitOfWork.Events.GetByIdAsync(eventId);
            if (eve == null) { return NotFound(); }
            if (eve.TicketsNumber <= 0)
            {
                return BadRequest();
            }
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _unitOfWork.Users.FindAsync(u => u.UserName.Equals(userName));
            if (await _unitOfWork.Tickets.FindAsync(t => t.EventId == eventId && t.UserId == user.Id) is not null) return BadRequest();
            var ticket = new Ticket
            {
                UserId = user.Id,
                EventId = eventId
            };
            eve.TicketsNumber--;
            await _unitOfWork.Tickets.AddAsync(ticket);
            _unitOfWork.Events.Update(eve);
            await _unitOfWork.SaveChangesAsync();
            return Ok("Booked successfully");
        }

        [Authorize]
        [HttpGet("CancelBook/{eventId}")]
        public async Task<ActionResult> CancelBook(int eventId)
        {
            var eve = await _unitOfWork.Events.GetByIdAsync(eventId);
            if (eve == null) { return NotFound(); }
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await _unitOfWork.Users.FindAsync(u => u.UserName.Equals(userName));
            var ticket = await _unitOfWork.Tickets.FindAsync(t => t.UserId == user.Id && t.EventId == eve.Id);
            if (ticket == null) { return BadRequest(); }
            _unitOfWork.Tickets.Delete(ticket);
            eve.TicketsNumber++;
            _unitOfWork.Events.Update(eve);
            await _unitOfWork.SaveChangesAsync();
            return NoContent();
        }

    }
}
