﻿using EventManagementSystem.Core.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.Servises
{
    public interface IAuthService
    {
        public Task<AuthDto> RegisterAsync(RegisterDto dto);
        public Task<AuthDto> LoginAsync(LoginDto dto);
    }
}
