﻿using EventManagementSystem.Core.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF
{
    public class EventManagementSystemDBContext: IdentityDbContext<User>
    {
        public EventManagementSystemDBContext(DbContextOptions<EventManagementSystemDBContext> options) : base(options)
        {
            
        }
        public DbSet<State> States { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
    }
}
