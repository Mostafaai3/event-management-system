﻿using EventManagementSystem.Core.Dto;
using EventManagementSystem.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.Repositories
{
    public class EventsRepository : GenericRepository<Event>, IEventsRepository
    {
        private readonly EventManagementSystemDBContext _context;
        public EventsRepository(EventManagementSystemDBContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Event>> GetBookedEvents(string id)
        {
            var events = await _context.Users
            .Where(u => u.Id == id)
            .SelectMany(p => p.Tickets.Select(u => u.Event))
            .ToListAsync();
            return events;
        }

        public async Task<EventDetailsDto> GetEventDetails(int id)
        {
            var eve = await _context.Events.FindAsync(id);
            var eveDetails = new EventDetailsDto
            {
                Id = eve.Id,
                Name = eve.Name,
                Description = eve.Description,
                Date = eve.Date,
                AdvertisingDate = eve.AdvertisingDate,
                CategoryId = eve.CategoryId,
                UserId = eve.UserId,
                LocationId = eve.LocationId,
                Price = eve.Price,
                TicketsNumber = eve.TicketsNumber,
                Latitude = eve.Latitude,
                Longitude = eve.Longitude,
            };
            eveDetails.Category = await _context.Categories.FindAsync(eve.CategoryId);
            eveDetails.Location = await _context.Locations.FindAsync(eve.LocationId);
            eveDetails.User = await _context.Users.FindAsync(eve.UserId);
            return eveDetails;
        }

        public async Task<IEnumerable<Event>> SearchEvent(SearchDto dto)
        {
            IQueryable<Event> query = _context.Events;
            if (dto.Categories != null)
            {
                foreach (var categoryId in dto.Categories)
                {
                    query = query.Where(e => e.CategoryId == categoryId);
                }
            }
            if (dto.Locations != null)
            {
                foreach (var locationId in dto.Locations)
                {
                    query = query.Where(e => e.LocationId == locationId);
                }
            }
            return await query.ToListAsync();
        }
    }
}
