﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        Task<T> GetByIdAsync(int id);
        Task<IEnumerable<T>> GetAllAsync(
            string[]? includes = null,
            Expression<Func<T, object>>? orderBy = null,
            string orderByDirection = "ASC");
        Task<T> FindAsync(
            Expression<Func<T, bool>> criteria,
            string[]? includes = null);
        Task<IEnumerable<T>> FindAllAsync(
            Expression<Func<T, bool>> criteria,
            string[]? includes = null,
            Expression<Func<T, object>>? orderBy = null,
            string orderByDirection = "ASC");
        Task<T> AddAsync(T entity);
        Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> entities);
        T Update(T entity);
        void Delete(T entity);
        void DeleteRange(IEnumerable<T> entities);
        Task<int> CountAsync();
        Task<int> CountAsync(Expression<Func<T, bool>> criteria);
    }
}
