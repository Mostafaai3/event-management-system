﻿using AutoMapper;
using EventManagementSystem.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.Repositories
{
    public class StatesRepository : GenericRepository<State>, IStatesRepository
    {
        private readonly EventManagementSystemDBContext _context;
        public StatesRepository(EventManagementSystemDBContext context) : base(context)
        {
            _context = context;
        }
        public async Task<IEnumerable<State>> GetAllLocationsAsync()
        {
            var states = await _context.States.Include(s => s.Locations).ToListAsync();
            return states;
        }
    }
}
