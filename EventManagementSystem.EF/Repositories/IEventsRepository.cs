﻿using EventManagementSystem.Core.Dto;
using EventManagementSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.Repositories
{
    public interface IEventsRepository : IGenericRepository<Event>
    {
        Task<EventDetailsDto> GetEventDetails(int id);
        public Task<IEnumerable<Event>> SearchEvent(SearchDto dto);
        public Task<IEnumerable<Event>> GetBookedEvents(string id);
    }
}
