﻿using EventManagementSystem.Core.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.Repositories
{
    public interface IStatesRepository: IGenericRepository<State>
    {
        Task<IEnumerable<State>> GetAllLocationsAsync();
    }
}
