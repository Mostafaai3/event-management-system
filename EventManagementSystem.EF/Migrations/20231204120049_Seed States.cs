﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EventManagementSystem.EF.Migrations
{
    /// <inheritdoc />
    public partial class SeedStates : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                            table: "States",
                            columns: new[] { "Id", "Name"},
                            values: new object[] { 1, "Damascus"}
                            );
            migrationBuilder.InsertData(
                            table: "States",
                            columns: new[] { "Id", "Name" },
                            values: new object[] { 2, "Daraa" }
                );
            migrationBuilder.InsertData(
                            table: "States",
                            columns: new[] { "Id", "Name" },
                            values: new object[] { 3, "Homs" }
                );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [States]");
        }
    }
}
