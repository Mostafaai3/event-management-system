﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EventManagementSystem.EF.Migrations
{
    /// <inheritdoc />
    public partial class SeedLocations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                            table: "Locations",
                            columns: new[] { "Id", "Name", "StateId" },
                            values: new object[] { 1, "Dael", 2 }
                            );
            migrationBuilder.InsertData(
                            table: "Locations",
                            columns: new[] { "Id", "Name", "StateId" },
                            values: new object[] { 2, "Tal Shihab", 2 }
                );
            migrationBuilder.InsertData(
                            table: "Locations",
                            columns: new[] { "Id", "Name", "StateId" },
                            values: new object[] { 3, "Al Mazzeh", 1 }
                );
            migrationBuilder.InsertData(
                            table: "Locations",
                            columns: new[] { "Id", "Name", "StateId" },
                            values: new object[] { 4, "Al Salhiyeh", 1 }
                );
            migrationBuilder.InsertData(
                            table: "Locations",
                            columns: new[] { "Id", "Name", "StateId" },
                            values: new object[] { 5, "AlHadhara", 3 }
                );
            migrationBuilder.InsertData(
                            table: "Locations",
                            columns: new[] { "Id", "Name", "StateId" },
                            values: new object[] { 6, "AlShammas", 3 }
                );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [Locations]");
        }
    }
}
