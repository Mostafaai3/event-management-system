﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EventManagementSystem.EF.Migrations
{
    /// <inheritdoc />
    public partial class SeedCategories : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                            table: "Categories",
                            columns: new[] { "Id", "Name" },
                            values: new object[] { 1, "Educational" }
                            );
            migrationBuilder.InsertData(
                            table: "Categories",
                            columns: new[] { "Id", "Name" },
                            values: new object[] { 2, "Sports" }
                );
            migrationBuilder.InsertData(
                            table: "Categories",
                            columns: new[] { "Id", "Name" },
                            values: new object[] { 3, "Social" }
                );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
