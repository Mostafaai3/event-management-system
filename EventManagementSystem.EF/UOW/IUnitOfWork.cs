﻿using EventManagementSystem.Core.Models;
using EventManagementSystem.EF.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.UOW
{
    public interface IUnitOfWork: IDisposable
    {
        IGenericRepository<Category> Categories { get; }
        IStatesRepository States { get; }
        IGenericRepository<Location> Locations { get; }
        IGenericRepository<User> Users { get; }
        IEventsRepository Events { get; }
        IGenericRepository<Ticket> Tickets { get; }
        Task<int> SaveChangesAsync();

    }
}
