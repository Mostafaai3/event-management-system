﻿using AutoMapper;
using EventManagementSystem.Core.Models;
using EventManagementSystem.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.EF.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EventManagementSystemDBContext _context;

        public UnitOfWork(EventManagementSystemDBContext context)
        {
            _context = context;
        }

        public IStatesRepository States => new StatesRepository(_context);

        public IGenericRepository<Location> Locations => new GenericRepository<Location>(_context);

        public IGenericRepository<User> Users => new GenericRepository<User>(_context);

        public IEventsRepository Events => new EventsRepository(_context);

        public IGenericRepository<Ticket> Tickets => new GenericRepository<Ticket>(_context);

        public IGenericRepository<Category> Categories => new GenericRepository<Category>(_context);

        public async void Dispose()
        {
            await _context.DisposeAsync();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
